# tatoda: the TAll TOwer DAtaset python API

API for downloading the data from the tall tower datasets.

[https://b2share.eudat.eu/records/0d3a99db75df4238820ee548f35ee36b]

### Example
```python
import xarray as xr
from tatoda import fetch_data

path_data = fetch_data('Malin head')
ds = xr.open_dataset(path_data)

print(ds)
```

Output:
```
<xarray.Dataset>
Dimensions:          (height: 3, latitude: 1, longitude: 1, time: 1577952)
Coordinates:
  * height           (height) float32 1.0 2.0 22.0
  * time             (time) datetime64[ns] 1988-01-01T00:05:00.000008940 ... 2017-12-31T23:55:00.073242220
  * latitude         (latitude) float32 55.35277
  * longitude        (longitude) float32 -7.332018
Data variables:
    wdiragl22S1      (time) float32 ...
    f_wdiragl22S1    (time) float32 ...
    wdiragl22S1_raw  (time) float32 ...
    windagl22S1      (time) float32 ...
    f_windagl22S1    (time) float32 ...
    windagl22S1_raw  (time) float32 ...
    psagl2S1         (time) float32 ...
    taagl1S1         (time) float32 ...
Attributes:
    tower_name:       malin_head
    institution:      Met Éireann
    boom_directions:  Not available
    location:         IE
    offshore:         no
    tower_type:       Met mast
    creation_time:    2019-06-08-T08:43:43Z
    links:            https://www.met.ie/climate/available-data/historical-data
    history:
```

### Requirements
 - requests
 - pandas
 - xarray

and their dependencies...

### Installation
```
pip install -e .  # From tatoda directory
```

### Removing cached files
Files are cached in `$HOME/.tatoda`. Remember to clear the files if they are no longer needed.

### Missing data from public index
All data from the public dataset: [https://b2share.eudat.eu/records/0d3a99db75df4238820ee548f35ee36b] is available through the tatoda API.

The following masts are present in the metadata file "0-INDEX_public.csv", but are not available in the publicly listed files:
 - Barro Colorado Island
 - Egmond aan zee
 - Ohio State University
 - WLEF

### Additonal references

The Tall Tower Dataset GMD paper is available from: https://www.earth-syst-sci-data-discuss.net/essd-2019-129/

The quality control procedure is described here: https://earth.bsc.es/gitlab/jramon/INDECIS-QCSS4TT/blob/master/vignettes/QCvignette.md
