#!/usr/bin/env python
from distutils.core import setup

setup(
    name='tatoda',
    version='0.0.1',
    author='Bjarke Tobias Olsen',
    author_email='btol@dtu.dk',
    packages=['tatoda'],
)
