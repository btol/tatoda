import numpy as np


def wind_speed(u, v):
    return np.sqrt(u*u + v*v)


def wind_direction(u, v):
    return 180.0 + np.arctan2(u, v) * 180.0/np.pi


def wind_speed_and_direction(u, v):
    return wind_speed(u, v), wind_direction(u, v)


def wind_vectors(ws, wd):
    return (-np.abs(ws)*np.sin(np.pi/180.0 * wd),
            -np.abs(ws)*np.cos(np.pi/180.0 * wd))
