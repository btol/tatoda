
def convert_tower_name(tower_name):
    tower_name = (tower_name.lower()
                            .replace(' ', '_'))

    if tower_name == 'rostamabad':
        tower_name = 'rostam_abad'
    return tower_name

