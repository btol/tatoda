from pathlib import Path
import zipfile
import io
import shutil

import requests
import pandas as pd
import xarray as xr

from .utils import convert_tower_name


PATH_DATA = Path.home() / '.tatoda'

# Version 1 May 13, 2019
# BASE_URL = 'https://b2share.eudat.eu/api/files/572f7d73-e13f-4e05-9715-fce93244e665/'

# Version 2 July 3, 2019
# BASE_URL = 'https://b2share.eudat.eu/api/files/d0437839-a6f9-4fd0-b779-f890ea11f9c6/'

# Version 3 Dec 19, 2019
# BASE_URL = 'https://b2share.eudat.eu/api/files/b75f2886-3b39-418c-9e54-e2e9177c3a39/'

# Version 4 
# BASE_URL = "https://b2share.eudat.eu/records/3ac9362f1cee49178236c9e03aec884d"
BASE_URL = "https://b2share.eudat.eu/api/files/c121aa6e-9a9d-4298-a61c-c4ff641ca1dc/"


if not PATH_DATA.is_dir():
    PATH_DATA.mkdir(parents=True, exist_ok=True)


def download_metadata():
    url = BASE_URL + '0-INDEX_public.csv'
    df = pd.read_csv(url)
    df.to_csv(PATH_DATA / '0-INDEX_public.csv', index=False)


def fetch_metadata():
    csv_file = PATH_DATA / '0-INDEX_public.csv'
    if not csv_file.is_file():
        download_metadata()
    return csv_file


def read_metadata():
    csv_file = fetch_metadata()
    return pd.read_csv(csv_file)


def download_and_combine_data(tower_name, path):

    path_tmp = PATH_DATA / tower_name
    url = BASE_URL + tower_name + '.zip'
    resp = requests.get(url, allow_redirects=True)

    with zipfile.ZipFile(io.BytesIO(resp.content)) as thezip:
        thezip.extractall(str(PATH_DATA))

    if tower_name == 'malin_head':
        shutil.rmtree(path_tmp / 'hourly')

    if tower_name == 'wm02':
        (path_tmp / '10minutely/huragl60S1/huragl60S1_201609.nc').unlink()

    def _preprocess(ds):
        return ds.sel(time=~ds.indexes['time'].duplicated())

    ds = xr.open_mfdataset(path_tmp.glob('**/*.nc'),
                           combine='by_coords',
                           data_vars='minimal',
                           preprocess=_preprocess)
    ds.load()
    ds.to_netcdf(path, compute=False)
    ds.close()

    shutil.rmtree(path_tmp)


def fetch_data(tower_name):
    tower_name = convert_tower_name(tower_name)
    netcdf_file = PATH_DATA / f'{tower_name}.nc'
    if not netcdf_file.is_file():
        download_and_combine_data(tower_name, netcdf_file)
    return netcdf_file


def read_data(tower_name):
    netcdf_file = fetch_data(tower_name)
    return xr.open_dataset(netcdf_file)
