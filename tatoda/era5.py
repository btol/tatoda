from pathlib import Path
import requests
import re

import numpy as np
import pandas as pd
import xarray as xr

from .utils import convert_tower_name
from .wind import wind_vectors, wind_speed_and_direction


URL_FMT = 'https://files.dtu.dk/fss/public/link/public/stream/read/{mast}-ERA5.nc?linkToken=iXU3olT1N6-bdTei&itemName=tatoda'
# URL_FMT = 'https://files.dtu.dk/fss/public/link/public/stream/read/{mast}-ERA5.nc'

PATH_DATA = Path.home() / '.tatoda' / 'ERA5'
if not PATH_DATA.is_dir():
    PATH_DATA.mkdir(parents=True, exist_ok=True)


def download_data(tower_name, netcdf_file):
    url = URL_FMT.format(mast=convert_tower_name(tower_name))
    resp = requests.get(url, allow_redirects=True)
    with open(netcdf_file, 'wb') as fobj:
        fobj.write(resp.content)


def fetch_data(tower_name):
    tower_name = convert_tower_name(tower_name)
    netcdf_file = PATH_DATA / f'{tower_name}-ERA5.nc'
    if not netcdf_file.is_file():
        download_data(tower_name, netcdf_file)
    return netcdf_file


def read_data(tower_name):
    netcdf_file = fetch_data(tower_name)
    return xr.open_dataset(netcdf_file)


def obs_wind_heights(obs):

    def _extract_height(s):
        m = re.search(r'(?<=windagl)\d+', s)
        return int(m.group(0))

    data_vars = obs.data_vars
    data_vars = [v for v in data_vars if 'windagl' in v]
    heights = np.array([_extract_height(v) for v in data_vars])
    return np.sort(np.unique(heights))


def interp_to_time_and_height(ds, time, height):
    if all(['WS' in ds.data_vars, 'WD' in ds.data_vars]):
        ds['U'], ds['V'] = wind_vectors(ds['WS'], ds['WD'])
        ds = ds.drop(['WS', 'WD'])
        ds = ds.interp(height=height, method='linear')
        ds = ds.interp(time=time, method='linear')
        ds['WS'], ds['WD'] = wind_speed_and_direction(ds['U'], ds['V'])
        ds = ds.drop(['U', 'V'])
    elif all(['U' in ds.data_vars, 'V' in ds.data_vars]):
        ds = ds.interp(time=time, height=height, method='linear')
    return ds


def make_era5_as_obs(era5, obs):

    day = pd.to_timedelta('1D')

    t0, t1 = obs.time.values.min(), obs.time.values.max()
    t0, t1 = pd.to_datetime(t0), pd.to_datetime(t1)
    t0, t1 = t0 - day, t1 + day

    era5 = era5.sel(time=slice(t0, t1)).load()
    wind_heights = obs_wind_heights(obs)
    era5 = interp_to_time_and_height(era5, obs.time.values, wind_heights)
    return era5
